import styled from "styled-components";
import { clickableCard } from "../shared/shared.styles";

export const CustomUnorderedList = styled.ul`
    list-style: none;
`;


export const ListItem = styled.li`
  ${clickableCard};
  cursor: pointer;
  margin: 1rem 0;
  
  &:hover {
    background-color: aliceblue;
  }
`;

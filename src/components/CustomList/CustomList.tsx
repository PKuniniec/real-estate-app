import React from 'react';

import { CustomUnorderedList, ListItem } from "./CustomList.styles";

const CustomList: React.FC<{ onOptionChose: (optionList: string) => void, listItems: string[]}> = (props) => {

    const getListItemHandler = (event: React.MouseEvent<HTMLLIElement>) => {
        props.onOptionChose(event.currentTarget.getAttribute('value') || '');
    }

    return (
        <CustomUnorderedList>
            {
                props.listItems.map(item =>
                    <ListItem key={Math.random().toString().split('.').join('')}
                                value={item}
                                onClick={getListItemHandler}
                    >
                        {item}
                    </ListItem>
                )
            }
        </CustomUnorderedList>
    )
}


export default CustomList;

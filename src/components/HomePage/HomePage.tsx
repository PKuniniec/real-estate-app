import React from 'react';
import NavForm from "../shared/NavForm/NavForm";
import { HeroContainer, BackgroundImage } from './HomePage.styles';


const LOCATIONS: string[] = ['Sydney', 'Melbourne', 'Adelaide', 'Brisbane'];
const PROPERTY_TYPES: string[] = ['Home', 'Unit', 'Land', 'Townhouse', 'Villa']



const HomePage: React.FC = () => (
    <>
        <HeroContainer>
            <BackgroundImage />
        </HeroContainer>
        <NavForm locations={LOCATIONS} types={PROPERTY_TYPES}/>
    </>
);

export default HomePage;

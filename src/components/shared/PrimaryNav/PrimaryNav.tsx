import React from 'react';
import {NavigationBar, OptionBox, OptionButton, OptionSwitcher} from "./PrimaryNav.styles";

const PrimaryNav: React.FC<{ onOptionChange: (option: string) => void, choseOption: string}> = (props) => {

    const selectButtonHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
        props.onOptionChange(event.currentTarget.value);
    }

    return (
        <NavigationBar>
            <OptionBox>
                <OptionSwitcher>
                    <OptionButton
                        onClick={selectButtonHandler}
                        value='buy'
                        className={`${props.choseOption === 'buy' && 'selected'}`}
                    >Buy Property
                    </OptionButton>
                    <OptionButton
                        onClick={selectButtonHandler}
                        value='rent'
                        className={`${props.choseOption === 'rent' && 'selected'}`}
                    >Rent Property</OptionButton>
                </OptionSwitcher>
            </OptionBox>
        </NavigationBar>
    )
};

export default PrimaryNav;

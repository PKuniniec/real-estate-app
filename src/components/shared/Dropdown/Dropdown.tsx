import React, {useState} from 'react';

import {
    BackPanel,
    DropdownGroup,
    DropdownContent,
    DropdownLabel,
    DropdownInput,
    DropdownLabelValue,
    ClearInputButton
} from './Dropdown.styles';
import CustomButton from "../CustomButtons/PrimaryButton";
import CancelCircle from "../SvgIcons/CancelCircle";

const Dropdown: React.FC<{
    titleText: string,
    isInput: boolean,
    label?: string,
    isError?: boolean,
    clearable?: boolean,
    onClearInput: () => void
}> = (props) => {

    const [isActive, toggleActive] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState<string>('');

    const placeNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputValue(event.target.value);
    }

    const toggleDropdown = () => {
        toggleActive(!isActive)
    }

    const clearInputHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation();
        props.onClearInput();
    }

    return (
        <>
            <BackPanel onClick={toggleDropdown} active={isActive}/>
            <DropdownGroup className="dropdown" active={isActive}>
                <DropdownLabel onClick={toggleDropdown}>
                    {props.titleText}
                    <DropdownLabelValue error={props.isError}>{!props.isInput && props?.label}</DropdownLabelValue>
                    <DropdownInput
                        displayInput={props.isInput}
                        value={inputValue}
                        placeholder={props?.label}
                        onChange={placeNameHandler}/>
                    <ClearInputButton isClearable={props.clearable}>
                        <CustomButton iconBtnWrapper type='button' onClick={clearInputHandler}>
                            <CancelCircle size={18}/>
                        </CustomButton>
                    </ClearInputButton>
                </DropdownLabel>
                {/*<ErrorIconContainer displayError={props.isError}>*/}
                {/*    <AlertCircleIcon size={24}/>*/}
                {/*</ErrorIconContainer>*/}
            </DropdownGroup>
            <DropdownContent active={isActive}>
                {props.children}
                <CustomButton buttonInverted type='button' onClick={toggleDropdown}>OK</CustomButton>
            </DropdownContent>
        </>
    )
};

export default Dropdown;

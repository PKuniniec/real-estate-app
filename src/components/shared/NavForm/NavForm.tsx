import React, {useEffect, useState} from 'react';
import {
    NavFormContainer,
    FormContainer,
    FormElement,
    PriceCounter,
    PriceAmount,
    LocationList
} from './NavForm.styles';

import Dropdown from "../Dropdown/Dropdown";
import PrimaryNav from "../PrimaryNav/PrimaryNav";
import MinusIconCircle from "../SvgIcons/MinusIconCircle";
import PlusIconCircle from "../SvgIcons/PlusIconCircle";
import CustomButton from "../CustomButtons/PrimaryButton";
import SearchIconSvg from "../SvgIcons/SearchIcon";
import ErrorMessage from "../ErrorMessage/ErrorMessage";
import CustomList from "../../CustomList/CustomList";


const NavForm: React.FC<{ locations: string[], types: string[]}> = (props) => {

    const [maxPrice, setMaxPrice] = useState<number>(0);
    const [minPrice, setMinPrice] = useState<number>(0);
    const [choseOption, setChoseOption] = useState('buy');
    const [location, setLocationOption] = useState('');
    const [property, setPropertyOption] = useState('');
    const [error, setError] = useState<{ isError: boolean, message: string }>({
        isError: false,
        message: ''
    });

    const navOptionHandler = (option: string) => {
        console.log(option);
        setChoseOption(() => option);
    }

    const clearPrice = () => {
        setMinPrice(0);
        setMaxPrice(0);
    }

    const priceHandler = (event: React.MouseEvent<HTMLButtonElement>) => {

        if (event.currentTarget.name === 'minPriceDown' && parseInt(event.currentTarget.value) > 0) {
            setMinPrice((prevValue?: number, nextValue?: number) => {
                if (choseOption === 'buy') {
                    nextValue = prevValue! - 50000;
                } else {
                    nextValue = prevValue! - 50;
                }
                return nextValue;
            })
        } else if (event.currentTarget.name === 'minPriceUp') {
            setMinPrice((prevValue?: number, nextValue?: number) => {
                if (choseOption === 'buy') {
                    nextValue = prevValue! + 50000;
                } else {
                    nextValue = prevValue! + 50;
                }
                return nextValue;
            })
        } else if (event.currentTarget.name === 'maxPriceDown' && parseInt(event.currentTarget.value) > 0) {
            setMaxPrice((prevValue?: number, nextValue?: number) => {
                if (choseOption === 'buy') {
                    nextValue = prevValue! - 50000;
                } else {
                    nextValue = prevValue! - 50;
                }
                return nextValue;
            })
        } else if (event.currentTarget.name === 'maxPriceUp') {
            setMaxPrice((prevValue?: number, nextValue?: number) => {
                if (choseOption === 'buy') {
                    nextValue = prevValue! + 50000;
                } else {
                    nextValue = prevValue! + 50;
                }
                return nextValue;
            })
        } else return;
    }

    const clearMinPriceHandler = () => {
        setMinPrice(0)
    }

    const clearMaxPriceHandler = () => {
        setMaxPrice(0)
    }

    const clearLocationHandler = () => {
        setLocationOption('')
    }

    const clearPropertyTypeHandler = () => {
        setPropertyOption('')
    }

    const getLocationHandler = (optionList: string) => {
        setLocationOption(optionList)
    }

    const getTypePropertyHandler = (optionList: string) => {
        setPropertyOption(optionList);
    }

    useEffect(() => {
        if (minPrice <= maxPrice || maxPrice === 0) {
            setError({
                isError: false,
                message: ''
            });
        } else if (minPrice > maxPrice && maxPrice !== 0) {
            setError({
                isError: true,
                message: 'Please provide correct price range.'
            })
        }

    }, [minPrice, maxPrice]);

    useEffect(() => {
        return () => {
            clearPrice();
        };
    }, [choseOption]);

    return (
        <NavFormContainer>
            <PrimaryNav onOptionChange={navOptionHandler} choseOption={choseOption}/>
            <FormContainer>
                <FormElement>
                    <Dropdown titleText='Location'
                              isInput={true}
                              clearable={location.length > 0}
                              label={`${location ? location : 'Enter location'}`}
                              onClearInput={clearLocationHandler}>
                        <LocationList>
                           <CustomList onOptionChose={getLocationHandler} listItems={props.locations} />
                        </LocationList>
                    </Dropdown>
                </FormElement>
                <FormElement>
                    <Dropdown titleText='Type of property:'
                              isInput={false}
                              clearable={property.length > 0}
                              label={`${property ? property: 'Add type'}`}
                              onClearInput={clearPropertyTypeHandler}>
                        <LocationList>
                            <CustomList onOptionChose={getTypePropertyHandler} listItems={props.types} />
                        </LocationList>
                    </Dropdown>
                </FormElement>
                <FormElement>
                    <Dropdown titleText={`${choseOption === 'buy' ? 'Min buy price' : 'Min rent price'}`}
                              isInput={false}
                              isError={error.isError}
                              clearable={minPrice > 0}
                              label={`${minPrice > 0 ? minPrice : 'Price from'}`}
                              onClearInput={clearMinPriceHandler}>
                        <PriceCounter>
                            <CustomButton
                                iconBtnWrapper
                                type={'button'}
                                name='minPriceDown'
                                value={minPrice}
                                disabled={minPrice === 0}
                                onClick={priceHandler}>
                                <MinusIconCircle size={24}/>
                            </CustomButton>
                            <PriceAmount>{minPrice}</PriceAmount>
                            <CustomButton
                                iconBtnWrapper
                                type={'button'}
                                name='minPriceUp'
                                value={minPrice}
                                onClick={priceHandler}>
                                <PlusIconCircle size={24}/>
                            </CustomButton>
                        </PriceCounter>
                        <ErrorMessage message={error.message}/>
                    </Dropdown>
                </FormElement>
                <FormElement>
                    <Dropdown
                        titleText={`${choseOption === 'buy' ? 'Max buy price' : 'Max rent price'}`}
                        isInput={false}
                        isError={error.isError}
                        clearable={maxPrice > 0}
                        label={`${maxPrice > 0 ? maxPrice : 'Price to'}`}
                        onClearInput={clearMaxPriceHandler}>
                        <PriceCounter>
                            {/* eslint-disable-next-line react/jsx-no-undef */}
                            <CustomButton
                                iconBtnWrapper
                                type={'button'}
                                name='maxPriceDown'
                                value={maxPrice}
                                disabled={maxPrice === 0}
                                onClick={priceHandler}>
                                <MinusIconCircle size={24}/>
                            </CustomButton>
                            <PriceAmount>{maxPrice}</PriceAmount>
                            <CustomButton
                                iconBtnWrapper
                                type={'button'}
                                name='maxPriceUp'
                                value={maxPrice}
                                onClick={priceHandler}>
                                <PlusIconCircle size={24}/>
                            </CustomButton>
                        </PriceCounter>
                        <ErrorMessage message={error.message}/>
                    </Dropdown>
                </FormElement>
                <FormElement searchButton>
                    <CustomButton searchBtn type='button'>
                        <SearchIconSvg size={16}/>
                    </CustomButton>
                </FormElement>

            </FormContainer>
        </NavFormContainer>
    )

}

export default NavForm;

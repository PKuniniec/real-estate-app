import React from "react";
import { ReactComponent as PlusIcon } from '../../../assets/plus_circle.svg';

const PlusIconCircle: React.FC<{ size: number}> = (props) => {
    return (
        <div style={{width: `${props.size}px`}}>
            <PlusIcon />
        </div>
    )
}

export default PlusIconCircle;

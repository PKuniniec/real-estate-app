import React from 'react';
import { ReactComponent as SearchIcon } from "../../../assets/search.svg";

const SearchIconSvg: React.FC<{ size: number }> = (props) => {
    return (
            <SearchIcon style={{ width: `${props.size}px`}}/>
    )
}

export default SearchIconSvg;

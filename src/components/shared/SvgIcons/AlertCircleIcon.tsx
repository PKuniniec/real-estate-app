import React from "react";
import { ReactComponent as AlertCircle } from "../../../assets/alert-circle.svg";


const AlertCircleIcon: React.FC<{ size: number }> = (props) => {
    return (
        <AlertCircle style={{  width: `${props.size}px`}} />
    )
}

export default AlertCircleIcon;

import React from "react";
import { ReactComponent as CancelCircleIcon} from "../../../assets/clear_circle.svg";


const CancelCircle: React.FC<{ size?: number }> = (props) => {
    return (
        <CancelCircleIcon style={{width: `${props.size}px`}}/>
    )
}

export default CancelCircle;

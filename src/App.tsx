import React from 'react';
import { GlobalStyle } from "./global.styles";

import './App.scss';
import {Route, Switch} from "react-router-dom";
import HomePage from "./components/HomePage/HomePage";

function App() {
  return (
    <div className="App">
      <GlobalStyle />

        <Switch>
            <Route path='/'>
                <HomePage />
            </Route>
        </Switch>
    </div>
  );
}

export default App;
